package org.springframework.internalrestapi;

import com.google.gson.Gson;
import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.*;
import org.springframework.internalrestapi.controller.UserController;
import org.springframework.internalrestapi.models.User;
import org.springframework.internalrestapi.repository.UserRepository;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.CopyOnWriteArrayList;


@SpringBootTest
public class UserControllerTests {

    private User correctuser = new User();
    private User wronguser = new User();

    private List<User> users = new CopyOnWriteArrayList<>();

    @Mock
    UserRepository userRepository;

    @InjectMocks
    UserController userController;

    Gson gson = new Gson();

    @Before
    public void start(){
        MockitoAnnotations.initMocks(this);
        correctuser.setId(Long.parseLong("1"));
        correctuser.setName("sid");
        correctuser.setEmail("sid@hotmail.nl");
        wronguser.setId(Long.parseLong("2"));
        wronguser.setName("nick");
        wronguser.setEmail("nick@hotmail.nl");
    }

    @Test
    void GetUserByIdReturn200(){
        Mockito.when(userRepository.findById(Long.parseLong("1"))).thenReturn(Optional.of(correctuser));
        ResponseEntity<?> res = userController.getUser(Long.parseLong("1"));

        int Code = res.getStatusCodeValue();
        Assert.assertEquals(200, Code);
    }

    @Test
    void GetUserByIdReturn204(){
        Mockito.when(userRepository.findById(Long.parseLong("1"))).thenReturn(Optional.of(correctuser));
        ResponseEntity<?> res = userController.getUser(Long.parseLong("2"));

        int Code = res.getStatusCodeValue();
        Assert.assertEquals(204, Code);
    }

    @Test
    void AddUserReturn200(){
        Mockito.when(userRepository.save(correctuser)).thenReturn(correctuser);
        ResponseEntity<?> res = userController.addUser("[" +gson.toJson(correctuser)+"]");

        int Code = res.getStatusCodeValue();
        Assert.assertEquals(200, Code);
    }

    @Test
    void AddUserReturn400(){
        Mockito.when(userRepository.save(correctuser)).thenReturn(correctuser);
        ResponseEntity<?> res = userController.addUser(gson.toJson(correctuser));

        int Code = res.getStatusCodeValue();
        Assert.assertEquals(400, Code);
    }

    @Test
    void DeleteUserReturn200(){
        Mockito.when(userRepository.findById(Long.parseLong("1"))).thenReturn(Optional.of(correctuser));
        ResponseEntity<?> res = userController.deleteUser(userRepository.findById(Long.parseLong("1")).get().getId());

        int Code = res.getStatusCodeValue();
        Assert.assertEquals(200, Code);
    }

    @Test
    void UpdateUserReturn200(){
        Mockito.when(userRepository.save(correctuser)).thenReturn(correctuser);
        ResponseEntity<?> res = userController.updateUser(gson.toJson(correctuser));

        int Code = res.getStatusCodeValue();
        Assert.assertEquals(200, Code);
    }
}
