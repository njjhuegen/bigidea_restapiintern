package org.springframework.internalrestapi;

import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.internalrestapi.controller.ClassesController;
import org.springframework.internalrestapi.models.Architype;
import org.springframework.internalrestapi.repository.ArchitypeRepository;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.CopyOnWriteArrayList;

import static org.junit.Assert.assertEquals;

@SpringBootTest
class ClassesControllerTests {

    private Architype correctclass = new Architype();
    private Architype wrongclass = new Architype();

    private List<Architype> correctlist = new CopyOnWriteArrayList<>();
    private List<Architype> wronglist = new CopyOnWriteArrayList<>();

    @Mock
    ArchitypeRepository architypeRepository;

    @InjectMocks
    ClassesController classesController;



    @Before
    void start(){
        MockitoAnnotations.initMocks(this);
        correctclass.setId(Long.parseLong("1"));
        correctclass.setHit_die(Long.parseLong("8"));
        correctclass.setName("monk");
        wrongclass.setId(Long.parseLong("2"));
        wrongclass.setId(Long.parseLong("12"));
        wrongclass.setName("barbarian");

    }

    @Test
    void GetAllClassesReturn200(){
        correctlist.add(correctclass);
        correctlist.add(wrongclass);
        Mockito.when(architypeRepository.findAll()).thenReturn(correctlist);
        ResponseEntity<?> res = classesController.getAllClasses();

        int Code = res.getStatusCodeValue();
        assertEquals(200, Code);
    }

    @Test
    void GetAllClassesReturn204(){
        Mockito.when(architypeRepository.findAll()).thenReturn(wronglist);
        ResponseEntity<?> res = classesController.getAllClasses();

        int Code = res.getStatusCodeValue();
        assertEquals(204, Code);
    }

    @Test
    void GetClassByIdReturn200(){
        Mockito.when(architypeRepository.findById(Long.parseLong("1"))).thenReturn(Optional.of(correctclass));
        ResponseEntity<?> res = classesController.getClassById(Long.parseLong("1"));

        int Code = res.getStatusCodeValue();
        assertEquals(200, Code);
    }

    @Test
    void GetClassByIdReturn204(){
        Mockito.when(architypeRepository.findById(Long.parseLong("2"))).thenReturn(Optional.of(wrongclass));
        ResponseEntity<?> res = classesController.getClassById(Long.parseLong("1"));

        int Code = res.getStatusCodeValue();
        assertEquals(204, Code);
    }

    @Test
    void GetClassByNameReturn200(){
        Mockito.when(architypeRepository.getClassByName("monk")).thenReturn(correctclass);
        ResponseEntity<?> res = classesController.getClassByName("monk");

        int Code = res.getStatusCodeValue();
        assertEquals(200, Code);
    }

    @Test
    void GetClassByNameReturn204(){
        Mockito.when(architypeRepository.getClassByName("monk")).thenReturn(correctclass);
        ResponseEntity<?> res = classesController.getClassByName("barbarian");

        int Code = res.getStatusCodeValue();
        assertEquals(204, Code);
    }
}
