package org.springframework.internalrestapi;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.internalrestapi.controller.ProficiencyController;
import org.springframework.internalrestapi.models.Proficiency;
import org.springframework.internalrestapi.repository.ProficiencyRepository;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.CopyOnWriteArrayList;

import static org.junit.Assert.assertEquals;

@SpringBootTest
public class ProficiencyControllerTests {
    private Proficiency proficiency = new Proficiency();
    private List<Proficiency> proficiencies = new CopyOnWriteArrayList<>();
    private List<Proficiency> wronglist = new CopyOnWriteArrayList<>();

    @Mock
    ProficiencyRepository proficiencyRepository;

    @InjectMocks
    ProficiencyController proficiencyController;

    @Before
    public void start(){
        MockitoAnnotations.initMocks(this);
        proficiency.setId(Long.parseLong("1"));
        proficiency.setName("light_armor");
        proficiency.setType("armor");
    }

    @Test
    public void GetAllProficiencyReturn200(){
        proficiencies.add(proficiency);
        Mockito.when(proficiencyRepository.findAll()).thenReturn(proficiencies);
        ResponseEntity<?> res = proficiencyController.getAllProfiencies();

        int Code = res.getStatusCodeValue();
        assertEquals(200, Code);
    }
    @Test

    public void GetAllProficiencyReturn204(){
        Mockito.when(proficiencyRepository.findAll()).thenReturn(wronglist);
        ResponseEntity<?> res = proficiencyController.getAllProfiencies();

        int Code = res.getStatusCodeValue();
        assertEquals(204, Code);
    }
    @Test
    public void GetProficiencyByIdReturn200(){
        Mockito.when(proficiencyRepository.findById(Long.parseLong("1"))).thenReturn(Optional.of(proficiency));
        ResponseEntity<?> res = proficiencyController.getProficiencyById(Long.parseLong("1"));

        int Code = res.getStatusCodeValue();
        assertEquals(200, Code);
    }

    @Test
    public void GetProficiencyByIdReturn204(){
        Mockito.when(proficiencyRepository.findById(Long.parseLong("1"))).thenReturn(Optional.of(proficiency));
        ResponseEntity<?> res = proficiencyController.getProficiencyById(Long.parseLong("2"));

        int Code = res.getStatusCodeValue();
        assertEquals(204, Code);
    }

    @Test
    public void GetProficiencyByNameReturn200(){
        Mockito.when(proficiencyRepository.getProficiencyByName("light_armor")).thenReturn(proficiency);
        ResponseEntity<?> res = proficiencyController.getProficiencyByName("light_armor");

        int Code = res.getStatusCodeValue();
        assertEquals(200, Code);
    }

    @Test
    public void GetProficiencyByNameReturn204(){
        Mockito.when(proficiencyRepository.getProficiencyByName("heavy_armor")).thenReturn(proficiency);
        ResponseEntity<?> res = proficiencyController.getProficiencyByName("light_armor");

        int Code = res.getStatusCodeValue();
        assertEquals(204, Code);
    }

}
