package org.springframework.internalrestapi;

import com.google.gson.Gson;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.internalrestapi.controller.CharacterController;
import org.springframework.internalrestapi.models.*;
import org.springframework.internalrestapi.repository.CharacterRepository;
import org.springframework.internalrestapi.repository.InventoryRepository;
import org.springframework.internalrestapi.repository.StatsCharacterRepository;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.CopyOnWriteArrayList;

import static org.junit.Assert.assertEquals;

@SpringBootTest
public class CharacterControllerTests {

    private CharacterModel correctchar = new CharacterModel();
    private CharacterModel wrongchar = new CharacterModel();
    private User user = new User();
    private User user2 = new User();
    private Inventory inv = new Inventory();
    private List<CharacterModel> correctlist = new CopyOnWriteArrayList<>();
    private List<Item> items = new CopyOnWriteArrayList<>();
    private StatsModel stat = new StatsModel();
    private Gson gson = new Gson();

    @Mock
    CharacterRepository characterRepository;
    @Mock
    StatsCharacterRepository statsCharacterRepository;
    @Mock
    InventoryRepository inventoryRepository;

    @InjectMocks
    CharacterController characterController;

    @Before
    public void start(){
        MockitoAnnotations.initMocks(this);
        user.setId(Long.parseLong("1"));
        user2.setId(Long.parseLong("2"));
        inv.setId(Long.parseLong("1"));
        inv.setCopper_amount(Long.parseLong("1"));
        inv.setGold_amount(Long.parseLong("1"));
        inv.setPlatinum_amount(Long.parseLong("1"));
        inv.setSilver_amount(Long.parseLong("1"));
        inv.setItems(items);
        stat.setCha_val(Long.parseLong("1"));
        stat.setWis_val(Long.parseLong("1"));
        stat.setCons_val(Long.parseLong("1"));
        stat.setStr_val(Long.parseLong("1"));
        stat.setInt_val(Long.parseLong("1"));
        stat.setDex_val(Long.parseLong("1"));
        stat.setId(Long.parseLong("1"));
        correctchar.setId(Long.parseLong("1"));
        correctchar.setUser_id(user);
        correctchar.setName("monk");
        correctchar.setXp(Long.parseLong("0"));
        correctchar.setAlignment("test");
        correctchar.setArchitype("test");
        correctchar.setBackground("test");
        correctchar.setCurrent_hp(Long.parseLong("2"));
        correctchar.setDeath_failure(Long.parseLong("1"));
        correctchar.setDeath_Succes(Long.parseLong("1"));
        correctchar.setInventory_id(inv);
        correctchar.setMax_hp(Long.parseLong("1"));
        correctchar.setRace("test");
        correctchar.setReligion("test");
        correctchar.setSubArchitype("test");
        correctchar.setStat_id(stat);
        wrongchar.setId(Long.parseLong("2"));
        wrongchar.setUser_id(user2);
        wrongchar.setName("barbarian");
    }

    @Test
    public void GetAllMyCharacterReturn200(){
        correctlist.add(correctchar);
        Mockito.when(characterRepository.findAllCharacters(correctchar.getUser_id().getId())).thenReturn(correctlist);
        ResponseEntity<?> res = characterController.getAllMyCharacters("1");

        int Code = res.getStatusCodeValue();
        assertEquals(200, Code);
    }

    @Test
    public void GetAllMyCharacterReturn204(){
        correctlist.add(correctchar);
        Mockito.when(characterRepository.findAllCharacters(correctchar.getUser_id().getId())).thenReturn(correctlist);
        ResponseEntity<?> res = characterController.getAllMyCharacters("2");

        int Code = res.getStatusCodeValue();
        assertEquals(204, Code);
    }

    @Test
    public void GetMyCharacterReturn200(){
        Mockito.when(characterRepository.getOneCharById(correctchar.getUser_id().getId(), correctchar.getId())).thenReturn(correctchar);
        ResponseEntity<?> res = characterController.getCharacter("1", "1");

        int Code = res.getStatusCodeValue();
        assertEquals(200, Code);
    }

    @Test
    public void GetMyCharacterReturn204(){
        Mockito.when(characterRepository.getOneCharById(correctchar.getUser_id().getId(), correctchar.getId())).thenReturn(correctchar);
        ResponseEntity<?> res = characterController.getCharacter("2", "1");

        int Code = res.getStatusCodeValue();
        assertEquals(204, Code);
    }

    @Test
    public void AddCharacterReturn200(){
        Mockito.when(characterRepository.save(correctchar)).thenReturn(correctchar);
        ResponseEntity<?> res = characterController.createCharacter(gson.toJson(correctchar));

        int Code = res.getStatusCodeValue();
        assertEquals(200, Code);
    }

    @Test
    public void AddCharacterReturn400(){
        Mockito.when(characterRepository.save(correctchar)).thenReturn(correctchar);
        ResponseEntity<?> res = characterController.createCharacter("[" +gson.toJson(correctchar)+ "]");

        int Code = res.getStatusCodeValue();
        assertEquals(400, Code);
    }

    @Test
    public void DeleteCharacterReturn200(){
        Mockito.when(characterRepository.findById(Long.parseLong("1"))).thenReturn(Optional.of(correctchar));
        ResponseEntity<?> res = characterController.deleteCharacter(characterRepository.findById(Long.parseLong("1")).get().getId());

        int Code = res.getStatusCodeValue();
        assertEquals(200, Code);
    }

    @Test
    public void UpdateCharacterReturn200(){
        Mockito.when(characterRepository.save(correctchar)).thenReturn(correctchar);
        ResponseEntity<?> res = characterController.updateCharacter(gson.toJson(correctchar));

        int Code = res.getStatusCodeValue();
        assertEquals(200, Code);
    }
}
