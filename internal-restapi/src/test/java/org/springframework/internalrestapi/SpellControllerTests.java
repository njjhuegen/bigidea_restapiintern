package org.springframework.internalrestapi;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.internalrestapi.controller.SpellController;
import org.springframework.internalrestapi.models.Architype;
import org.springframework.internalrestapi.models.Spell;
import org.springframework.internalrestapi.repository.SpellRepository;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.CopyOnWriteArrayList;

import static org.junit.Assert.assertEquals;

@SpringBootTest
public class SpellControllerTests {
    private Spell spell = new Spell();
    private List<Spell> spells = new CopyOnWriteArrayList<>();
    private List<Spell> wronglist = new CopyOnWriteArrayList<>();

    @Mock
    SpellRepository spellRepository;

    @InjectMocks
    SpellController spellController;

    @Before
    public void start(){
        MockitoAnnotations.initMocks(this);
        List<Architype> test = new CopyOnWriteArrayList<>();
        spell.setArchitypes(test);
        spell.setId(Long.parseLong("1"));
        spell.setLevel(Long.parseLong("1"));
        spell.setCasting_time("2.5");
        spell.setComponents("test");
        spell.setDescription("woop");
        spell.setDuration("5");
        spell.setHigher_levels("10");
        spell.setName("firebolt");
        spell.setSpell_range("40");
        spell.setType("fire");
    }

    @Test
    public void GetAllStatusAilmentReturn200(){
        spells.add(spell);
        Mockito.when(spellRepository.findAll()).thenReturn(spells);
        ResponseEntity<?> res = spellController.getAllSpells();

        int Code = res.getStatusCodeValue();
        assertEquals(200, Code);
    }
    @Test

    public void GetAllStatusAilmentReturn204(){
        Mockito.when(spellRepository.findAll()).thenReturn(wronglist);
        ResponseEntity<?> res = spellController.getAllSpells();

        int Code = res.getStatusCodeValue();
        assertEquals(204, Code);
    }
    @Test
    public void GetStatusAilmentByIdReturn200(){
        Mockito.when(spellRepository.findById(Long.parseLong("1"))).thenReturn(Optional.of(spell));
        ResponseEntity<?> res = spellController.getSpellById(Long.parseLong("1"));

        int Code = res.getStatusCodeValue();
        assertEquals(200, Code);
    }

    @Test
    public void GetStatusAilmentByIdReturn204(){
        Mockito.when(spellRepository.findById(Long.parseLong("1"))).thenReturn(Optional.of(spell));
        ResponseEntity<?> res = spellController.getSpellById(Long.parseLong("2"));

        int Code = res.getStatusCodeValue();
        assertEquals(204, Code);
    }

    @Test
    public void GetStatusAilmentByNameReturn200(){
        Mockito.when(spellRepository.getSpellByName("firebolt")).thenReturn(spell);
        ResponseEntity<?> res = spellController.getSpellByName("firebolt");

        int Code = res.getStatusCodeValue();
        assertEquals(200, Code);
    }

    @Test
    public void GetStatusAilmentByNameReturn204(){
        Mockito.when(spellRepository.getSpellByName("firebolt")).thenReturn(spell);
        ResponseEntity<?> res = spellController.getSpellByName("pois");

        int Code = res.getStatusCodeValue();
        assertEquals(204, Code);
    }
}
