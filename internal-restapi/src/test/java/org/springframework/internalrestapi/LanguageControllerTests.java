package org.springframework.internalrestapi;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.internalrestapi.controller.LanguageController;
import org.springframework.internalrestapi.models.Language;
import org.springframework.internalrestapi.repository.LanguageRepository;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.CopyOnWriteArrayList;

import static org.junit.Assert.assertEquals;

@SpringBootTest
public class LanguageControllerTests {

    private Language language = new Language();
    private List<Language> languages = new CopyOnWriteArrayList<>();
    private List<Language> wronglist = new CopyOnWriteArrayList<>();

    @Mock
    LanguageRepository languageRepository;

    @InjectMocks
    LanguageController languageController;

    @Before
    public void start(){
        MockitoAnnotations.initMocks(this);
        language.setId(Long.parseLong("1"));
        language.setName("common");
        language.setScript("human");
        language.setType("human");
    }

    @Test
    public void GetAllLanguageReturn200(){
        languages.add(language);
        Mockito.when(languageRepository.findAll()).thenReturn(languages);
        ResponseEntity<?> res = languageController.getAllLang();

        int Code = res.getStatusCodeValue();
        assertEquals(200, Code);
    }
    @Test

    public void GetAllLanguageReturn204(){
        Mockito.when(languageRepository.findAll()).thenReturn(wronglist);
        ResponseEntity<?> res = languageController.getAllLang();

        int Code = res.getStatusCodeValue();
        assertEquals(204, Code);
    }
    @Test
    public void GetLanguageByIdReturn200(){
        Mockito.when(languageRepository.findById(Long.parseLong("1"))).thenReturn(Optional.of(language));
        ResponseEntity<?> res = languageController.getLangById(Long.parseLong("1"));

        int Code = res.getStatusCodeValue();
        assertEquals(200, Code);
    }

    @Test
    public void GetLanguageByIdReturn204(){
        Mockito.when(languageRepository.findById(Long.parseLong("1"))).thenReturn(Optional.of(language));
        ResponseEntity<?> res = languageController.getLangById(Long.parseLong("2"));

        int Code = res.getStatusCodeValue();
        assertEquals(204, Code);
    }

    @Test
    public void GetLanguageByNameReturn200(){
        Mockito.when(languageRepository.getLangByName("common")).thenReturn(language);
        ResponseEntity<?> res = languageController.getLangByName("common");

        int Code = res.getStatusCodeValue();
        assertEquals(200, Code);
    }

    @Test
    public void GetLanguageByNameReturn204(){
        Mockito.when(languageRepository.getLangByName("demonic")).thenReturn(language);
        ResponseEntity<?> res = languageController.getLangByName("common");

        int Code = res.getStatusCodeValue();
        assertEquals(204, Code);
    }
}
