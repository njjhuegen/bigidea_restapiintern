package org.springframework.internalrestapi;

import com.google.gson.Gson;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.internalrestapi.controller.ItemController;
import org.springframework.internalrestapi.models.Item;
import org.springframework.internalrestapi.repository.ItemRepository;


import java.util.List;
import java.util.Optional;
import java.util.concurrent.CopyOnWriteArrayList;

import static org.junit.Assert.assertEquals;

@SpringBootTest
public class ItemControllerTests {

    private Item item = new Item();
    private List<Item> items = new CopyOnWriteArrayList<>();
    private List<Item> wronglist = new CopyOnWriteArrayList<>();
    private Gson gson = new Gson();

    @Mock
    ItemRepository itemRepository;

    @InjectMocks
    ItemController itemController;

    @Before
    public void start(){
        MockitoAnnotations.initMocks(this);
        item.setId(Long.parseLong("1"));
        item.setName("test");
    }

    @Test
    public void GetAllItemsByIdReturn200(){
        items.add(item);
        Mockito.when(itemRepository.findAll()).thenReturn(items);
        ResponseEntity<?> res = itemController.getAllItems();

        int Code = res.getStatusCodeValue();
        assertEquals(200, Code);
    }

    @Test
    public void GetAllItemsByIdReturn204(){
        items.add(item);
        Mockito.when(itemRepository.findAll()).thenReturn(wronglist);
        ResponseEntity<?> res = itemController.getAllItems();

        int Code = res.getStatusCodeValue();
        assertEquals(204, Code);
    }

    @Test
    public void GetItemByIdReturn200(){
        Mockito.when(itemRepository.findById(Long.parseLong("1"))).thenReturn(Optional.of(item));
        ResponseEntity<?> res = itemController.getItem(Long.parseLong("1"));

        int Code = res.getStatusCodeValue();
        assertEquals(200, Code);
    }

    @Test
    public void GetItemByIdReturn204(){
        Mockito.when(itemRepository.findById(Long.parseLong("1"))).thenReturn(Optional.of(item));
        ResponseEntity<?> res = itemController.getItem(Long.parseLong("2"));

        int Code = res.getStatusCodeValue();
        assertEquals(204, Code);
    }

    @Test
    public void AddItemReturn200(){
        Mockito.when(itemRepository.save(item)).thenReturn(item);
        ResponseEntity<?> res = itemController.addItem(gson.toJson(item));

        int Code = res.getStatusCodeValue();
        assertEquals(200, Code);
    }

    @Test
    public void AddItemReturn400(){
        Mockito.when(itemRepository.save(item)).thenReturn(item);
        ResponseEntity<?> res = itemController.addItem("[" +gson.toJson(item)+"]");

        int Code = res.getStatusCodeValue();
        assertEquals(400, Code);
    }

    @Test
    public void DeleteItemReturn200(){
        Mockito.when(itemRepository.findById(Long.parseLong("1"))).thenReturn(Optional.of(item));
        ResponseEntity<?> res = itemController.deleteItem(gson.toJson(item));

        int Code = res.getStatusCodeValue();
        assertEquals(200, Code);
    }

    @Test
    public void UpdateItemReturn200(){
        Mockito.when(itemRepository.save(item)).thenReturn(item);
        ResponseEntity<?> res = itemController.updateItem(gson.toJson(item));

        int Code = res.getStatusCodeValue();
        assertEquals(200, Code);
    }
}
