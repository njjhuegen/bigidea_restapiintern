package org.springframework.internalrestapi;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.internalrestapi.controller.StatusAilmentsController;
import org.springframework.internalrestapi.models.StatusAilment;
import org.springframework.internalrestapi.repository.StatusAilmentRepository;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.CopyOnWriteArrayList;

import static org.junit.Assert.assertEquals;

@SpringBootTest
public class StatusAilmentsControllerTests {
    private StatusAilment statusAilment = new StatusAilment();
    private List<StatusAilment> statusAilments = new CopyOnWriteArrayList<>();
    private List<StatusAilment> wronglist = new CopyOnWriteArrayList<>();

    @Mock
    StatusAilmentRepository statusAilmentRepository;

    @InjectMocks
    StatusAilmentsController statusAilmentsController;

    @Before
    public void start(){
        MockitoAnnotations.initMocks(this);
        statusAilment.setId(Long.parseLong("1"));
        statusAilment.setName("poison");
        statusAilment.setDiceamount(Long.parseLong("4"));
        statusAilment.setDicedamage(Long.parseLong("4"));
        statusAilment.setDicetype(Long.parseLong("20"));
        statusAilment.setLevel(Long.parseLong("3"));
    }

    @Test
    public void GetAllStatusAilmentReturn200(){
        statusAilments.add(statusAilment);
        Mockito.when(statusAilmentRepository.findAll()).thenReturn(statusAilments);
        ResponseEntity<?> res = statusAilmentsController.getAllAilments();

        int Code = res.getStatusCodeValue();
        assertEquals(200, Code);
    }
    @Test

    public void GetAllStatusAilmentReturn204(){
        Mockito.when(statusAilmentRepository.findAll()).thenReturn(wronglist);
        ResponseEntity<?> res = statusAilmentsController.getAllAilments();

        int Code = res.getStatusCodeValue();
        assertEquals(204, Code);
    }
    @Test
    public void GetStatusAilmentByIdReturn200(){
        Mockito.when(statusAilmentRepository.findById(Long.parseLong("1"))).thenReturn(Optional.of(statusAilment));
        ResponseEntity<?> res = statusAilmentsController.getAilmentById(Long.parseLong("1"));

        int Code = res.getStatusCodeValue();
        assertEquals(200, Code);
    }

    @Test
    public void GetStatusAilmentByIdReturn204(){
        Mockito.when(statusAilmentRepository.findById(Long.parseLong("1"))).thenReturn(Optional.of(statusAilment));
        ResponseEntity<?> res = statusAilmentsController.getAilmentById(Long.parseLong("2"));

        int Code = res.getStatusCodeValue();
        assertEquals(204, Code);
    }

    @Test
    public void GetStatusAilmentByNameReturn200(){
        Mockito.when(statusAilmentRepository.getStatusailmentByName("poison")).thenReturn(statusAilment);
        ResponseEntity<?> res = statusAilmentsController.getAilmentByName("poison");

        int Code = res.getStatusCodeValue();
        assertEquals(200, Code);
    }

    @Test
    public void GetStatusAilmentByNameReturn204(){
        Mockito.when(statusAilmentRepository.getStatusailmentByName("poison")).thenReturn(statusAilment);
        ResponseEntity<?> res = statusAilmentsController.getAilmentByName("pois");

        int Code = res.getStatusCodeValue();
        assertEquals(204, Code);
    }
}
