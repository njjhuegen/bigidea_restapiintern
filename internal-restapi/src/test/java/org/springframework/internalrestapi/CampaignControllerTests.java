package org.springframework.internalrestapi;

import com.google.gson.Gson;
import io.netty.handler.codec.http.HttpResponseStatus;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.internalrestapi.controller.CampaignController;
import org.springframework.internalrestapi.models.Campaign;
import org.springframework.internalrestapi.models.CharacterModel;
import org.springframework.internalrestapi.models.User;
import org.springframework.internalrestapi.repository.CampaignRepository;

import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.OutputKeys;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CopyOnWriteArrayList;

import static org.junit.Assert.assertEquals;

@SpringBootTest
public class CampaignControllerTests {
    private Campaign correctcampaign = new Campaign();
    private User user = new User();
    private CharacterModel player1 = new CharacterModel();
    private List<Campaign> campaigns = new CopyOnWriteArrayList<>();
    private List<CharacterModel> players = new CopyOnWriteArrayList<>();
    private Gson gson = new Gson();
    private HttpServletResponse result;

    @Mock
    CampaignRepository campaignRepository;

    @InjectMocks
    CampaignController campaignController;

    @Before
    public void start(){
        MockitoAnnotations.initMocks(this);
        players.add(player1);
        user.setId(Long.parseLong("1"));
        correctcampaign.setId(Long.parseLong("1"));
        correctcampaign.setDm(user);
        correctcampaign.setCharacters(players);
    }

    @Test
    public void GetAllMyCampaignReturn200(){
        campaigns.add(correctcampaign);
        Mockito.when(campaignRepository.findAllMyCampaignsByUserId(user.getId())).thenReturn(campaigns);
        ResponseEntity<?> res = campaignController.getAllMyCampaigns("1");

        int Code = res.getStatusCodeValue();
        assertEquals(200, Code);
    }

    @Test
    public void GetAllMyCampaignReturn204(){
        campaigns.add(correctcampaign);
        Mockito.when(campaignRepository.findAllMyCampaignsByUserId(user.getId())).thenReturn(campaigns);
        ResponseEntity<?> res = campaignController.getAllMyCampaigns("2");

        int Code = res.getStatusCodeValue();
        assertEquals(204, Code);
    }

    @Test
    public void GetMyCampaignReturn200(){
        Mockito.when(campaignRepository.getOneByCampaignId(user.getId(), correctcampaign.getId())).thenReturn(correctcampaign);
        ResponseEntity<?> res = campaignController.getCampaign(Long.parseLong("1"), Long.parseLong("1"));

        int Code = res.getStatusCodeValue();
        assertEquals(200, Code);
    }

    @Test
    public void GetMyCampaignReturn204(){
        Mockito.when(campaignRepository.getOneByCampaignId(user.getId(), correctcampaign.getId())).thenReturn(correctcampaign);
        ResponseEntity<?> res = campaignController.getCampaign(Long.parseLong("2"), Long.parseLong("1"));

        int Code = res.getStatusCodeValue();
        assertEquals(204, Code);
    }

    @Test
    public void AddCharacterReturn200(){
        Mockito.when(campaignRepository.save(correctcampaign)).thenReturn(correctcampaign);
        ResponseEntity<?> res = campaignController.createCampaign(gson.toJson(correctcampaign));

        int Code = res.getStatusCodeValue();
        assertEquals(200, Code);
    }

    @Test
    public void AddCharacterReturn400(){
        Mockito.when(campaignRepository.save(correctcampaign)).thenReturn(correctcampaign);
        ResponseEntity<?> res = campaignController.createCampaign("[" +gson.toJson(correctcampaign)+ "]");

        int Code = res.getStatusCodeValue();
        assertEquals(400, Code);
    }

    @Test
    public void DeleteCharacterReturn200(){
        Mockito.when(campaignRepository.findById(Long.parseLong("1"))).thenReturn(Optional.of(correctcampaign));
        ResponseEntity<?> res = campaignController.deleteCampaign(gson.toJson(correctcampaign));

        int Code = res.getStatusCodeValue();
        assertEquals(200, Code);
    }

    @Test
    public void UpdateCharacterReturn200(){
        Mockito.when(campaignRepository.save(correctcampaign)).thenReturn(correctcampaign);
        ResponseEntity<?> res = campaignController.updateCampaign(gson.toJson(correctcampaign));

        int Code = res.getStatusCodeValue();
        assertEquals(200, Code);

    }
}
