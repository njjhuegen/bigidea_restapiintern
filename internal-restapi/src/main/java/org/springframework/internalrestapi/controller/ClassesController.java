package org.springframework.internalrestapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.internalrestapi.models.Architype;
import org.springframework.internalrestapi.repository.ArchitypeRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:8080")
@RestController
@RequestMapping("/classes")
public class ClassesController {
    @Autowired
    ArchitypeRepository architypeRepository;

    @GetMapping("")
    public @ResponseBody ResponseEntity getAllClasses() {
        List<Architype> archetypes;
        try{
            archetypes = architypeRepository.findAll();
            if(archetypes.isEmpty()){
                return new ResponseEntity(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(archetypes, HttpStatus.OK);
        } catch (Exception ex){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/{classid}")
    public @ResponseBody ResponseEntity getClassById(@PathVariable Long classid) {
        Architype architype;
        try{
            if(architypeRepository.findById(classid).isPresent()){
                architype = architypeRepository.findById(classid).get();
                return new ResponseEntity<>(architype, HttpStatus.OK);
            }
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        catch (Exception ex){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/name/{classid}")
    public @ResponseBody ResponseEntity  getClassByName(@PathVariable String classid) {
        Architype architype;
        try{
            architype = architypeRepository.getClassByName(classid);
            if(architype == null){
                return new ResponseEntity(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(architype, HttpStatus.OK);
        } catch (Exception ex){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }
}
