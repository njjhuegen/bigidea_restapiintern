package org.springframework.internalrestapi.controller;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.internalrestapi.models.CharacterModel;
import org.springframework.internalrestapi.repository.CampaignRepository;
import org.springframework.internalrestapi.repository.CharacterRepository;
import org.springframework.internalrestapi.repository.InventoryRepository;
import org.springframework.internalrestapi.repository.StatsCharacterRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/character")
public class CharacterController {
    @Autowired
    private CharacterRepository characterRepository;
    @Autowired
    private InventoryRepository inventoryRepository;
    @Autowired
    private StatsCharacterRepository statsCharacterRepository;
    private Gson gson = new Gson();

    @GetMapping("/{user}")
    public @ResponseBody ResponseEntity getAllMyCharacters(@PathVariable String user) {
        List<CharacterModel> characters;
        try{
            characters = characterRepository.findAllCharacters(Long.parseLong(user));
            if(characters.isEmpty()){
                return new ResponseEntity(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(characters, HttpStatus.OK);
        } catch (Exception ex){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/{user}/{character}")
    public @ResponseBody ResponseEntity getCharacter(@PathVariable String user, @PathVariable String character) {
        CharacterModel newCharacter;
        try {
            newCharacter = characterRepository.getOneCharById(Long.parseLong(user), Long.parseLong(character));
            if(newCharacter == null){
                return new ResponseEntity(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(newCharacter, HttpStatus.OK);
        } catch (Exception ex){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @PatchMapping("")
    public @ResponseBody ResponseEntity updateCharacter(@RequestBody String character) {
        try{
            CharacterModel characterModel = gson.fromJson(character, CharacterModel.class);
            characterRepository.save(characterModel);
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception ex){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("{id}")
    public @ResponseBody ResponseEntity deleteCharacter(@PathVariable Long id) {
        CharacterModel newCharacter;
        try{
            newCharacter = characterRepository.findById(id).get();
            prepareDeleteCharacter(newCharacter);
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception ex){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("")
    public @ResponseBody ResponseEntity createCharacter(@RequestBody String character){
        try{
            CharacterModel characterModel = gson.fromJson(character, CharacterModel.class);
            prepareCharacter(characterModel);
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception ex){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    private void prepareCharacter(CharacterModel characterModel){
        inventoryRepository.save(characterModel.getInventory_id());
        statsCharacterRepository.save(characterModel.getStat_id());
        characterRepository.save(characterModel);
    }

    private void prepareDeleteCharacter(CharacterModel characterModel){
        inventoryRepository.deleteById(characterModel.getInventory_id().getId());
        statsCharacterRepository.deleteById(characterModel.getStat_id().getId());
        characterRepository.deleteById(characterModel.getId());
    }
}
