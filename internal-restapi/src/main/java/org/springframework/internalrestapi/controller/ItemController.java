package org.springframework.internalrestapi.controller;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.internalrestapi.models.Item;
import org.springframework.internalrestapi.repository.ItemRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:8080")
@RequestMapping("/item")
public class ItemController {
    @Autowired
    ItemRepository itemRepository;
    Gson gson = new Gson();

    @GetMapping("")
    public @ResponseBody ResponseEntity getAllItems(){
        List<Item> items;
        try{
            items = itemRepository.findAll();
            if(items.isEmpty()){
                return new ResponseEntity(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(items, HttpStatus.OK);
        } catch (Exception ex){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/{itemid}")
    public @ResponseBody ResponseEntity getItem(@PathVariable Long itemid){
        Item item;
        try{
            if(itemRepository.findById(itemid).isPresent()){
                item = itemRepository.findById(itemid).get();
                return new ResponseEntity<>(item, HttpStatus.OK);
            }
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        } catch(Exception ex) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("")
    public @ResponseBody ResponseEntity addItem(@RequestParam String item){
        try{
            itemRepository.save(gson.fromJson(item, Item.class));
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception ex){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("")
    public @ResponseBody ResponseEntity deleteItem(@RequestBody String item){
        try{
            Item itemModel = gson.fromJson(item, Item.class);
            itemRepository.deleteById(itemModel.getId());
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception ex){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @PatchMapping("")
    public @ResponseBody ResponseEntity updateItem(@RequestBody String item){
        try {
            itemRepository.save(gson.fromJson(item, Item.class));
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception ex){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }
}
