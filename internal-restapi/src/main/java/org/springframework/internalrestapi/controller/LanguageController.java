package org.springframework.internalrestapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.internalrestapi.models.Language;
import org.springframework.internalrestapi.repository.LanguageRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/language")
public class LanguageController {
    @Autowired
    LanguageRepository languageRepository;

    @GetMapping("")
    public @ResponseBody ResponseEntity getAllLang(){
        List<Language> languages;
        try{
            languages = languageRepository.findAll();
            if(languages.isEmpty()){
                return new ResponseEntity(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(languages, HttpStatus.OK);
        } catch (Exception ex){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/{languageid}")
    public @ResponseBody ResponseEntity getLangById(@PathVariable Long languageid){
        Language language;
        try{
            if(languageRepository.findById(languageid).isPresent()){
                language = languageRepository.findById(languageid).get();
                return new ResponseEntity<>(language, HttpStatus.OK);
            }
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        catch (Exception ex){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/name/{languageid}")
    public @ResponseBody ResponseEntity getLangByName(@PathVariable String languageid){
        Language language;
        try{
            language = languageRepository.getLangByName(languageid);
            if(language != null){
                return new ResponseEntity<>(language, HttpStatus.OK);
            }
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        catch (Exception ex){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }
}
