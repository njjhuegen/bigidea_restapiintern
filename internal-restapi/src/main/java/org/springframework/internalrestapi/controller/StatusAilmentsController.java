package org.springframework.internalrestapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.internalrestapi.models.StatusAilment;
import org.springframework.internalrestapi.repository.StatusAilmentRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("statusailments")
public class StatusAilmentsController {
    @Autowired
    StatusAilmentRepository statusAilmentRepository;

    @GetMapping("")
    public @ResponseBody ResponseEntity getAllAilments(){
        List<StatusAilment> statusAilment;
        try{
            statusAilment = statusAilmentRepository.findAll();
            if(statusAilment.isEmpty()){
                return new ResponseEntity(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(statusAilment, HttpStatus.OK);
        }
        catch (Exception ex){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/{ailmentid}")
    public @ResponseBody ResponseEntity getAilmentById(@PathVariable Long ailmentid){
        StatusAilment statusAilment;
        try{
            if(statusAilmentRepository.findById(ailmentid).isPresent()){
                statusAilment = statusAilmentRepository.findById(ailmentid).get();
                return new ResponseEntity<>(statusAilment, HttpStatus.OK);
            }
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        catch (Exception ex){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/name/{ailmentid}")
    public @ResponseBody ResponseEntity getAilmentByName(@PathVariable String ailmentid){
        StatusAilment statusAilment;
        try{
            statusAilment = statusAilmentRepository.getStatusailmentByName(ailmentid);
            if(statusAilment != null){
                return new ResponseEntity<>(statusAilment, HttpStatus.OK);
            }
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        catch (Exception ex){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

}
