package org.springframework.internalrestapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.internalrestapi.models.Spell;
import org.springframework.internalrestapi.repository.SpellRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/spell")
public class SpellController {
    @Autowired
    SpellRepository spellRepository;

    @GetMapping("")
    public @ResponseBody ResponseEntity getAllSpells(){
        List<Spell> spells;
        try{
            if(!spellRepository.findAll().isEmpty()){
                spells = spellRepository.findAll();
                return new ResponseEntity<>(spells, HttpStatus.OK);
            }
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        catch (Exception ex){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/{spellid}")
    public @ResponseBody
    ResponseEntity getSpellById(@PathVariable Long spellid){
        Spell spell;
        try{
            if(spellRepository.findById(spellid).isPresent()){
                spell = spellRepository.findById(spellid).get();
                return new ResponseEntity<>(spell, HttpStatus.OK);
            }
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        catch (Exception ex){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/name/{spellid}")
    public @ResponseBody ResponseEntity getSpellByName(@PathVariable String spellid){
        Spell spell;
        try{
            spell = spellRepository.getSpellByName(spellid);
            if(spell != null){
                return new ResponseEntity<>(spell, HttpStatus.OK);
            }
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        catch (Exception ex){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

}
