package org.springframework.internalrestapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.internalrestapi.models.*;
import org.springframework.internalrestapi.repository.ProficiencyRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/proficiency")
public class ProficiencyController {
    @Autowired
    ProficiencyRepository profiencyRepository;

    @GetMapping("")
    public @ResponseBody ResponseEntity getAllProfiencies(){
        List<Proficiency> profiencies;
        try{
            if(!profiencyRepository.findAll().isEmpty()){
                profiencies = profiencyRepository.findAll();
                return new ResponseEntity<>(profiencies, HttpStatus.OK);
            }
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        catch (Exception ex){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/{proficiencyid}")
    public @ResponseBody ResponseEntity getProficiencyById(@PathVariable Long proficiencyid){
        Proficiency proficiency;
        try{
            if(profiencyRepository.findById(proficiencyid).isPresent()){
                proficiency = profiencyRepository.findById(proficiencyid).get();
                return new ResponseEntity<>(proficiency, HttpStatus.OK);
            }
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        catch (Exception ex){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("/name/{proficiencyid}")
    public @ResponseBody ResponseEntity getProficiencyByName(@PathVariable String proficiencyid){
        Proficiency proficiency;
        try{
            proficiency = profiencyRepository.getProficiencyByName(proficiencyid);
            if(proficiency != null){
                return new ResponseEntity<>(proficiency, HttpStatus.OK);
            }
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        catch (Exception ex){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }
}
