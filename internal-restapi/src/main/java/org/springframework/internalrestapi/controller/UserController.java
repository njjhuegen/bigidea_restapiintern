package org.springframework.internalrestapi.controller;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.internalrestapi.models.User;
import org.springframework.internalrestapi.repository.UserRepository;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "http://localhost:8080")
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserRepository userRepository;
    private Gson gson = new Gson();

    @GetMapping("/{userId}")
    public @ResponseBody ResponseEntity getUser(@PathVariable Long userId){
        User user;
        try{
            if(userRepository.findById(userId).isPresent()){
                user = userRepository.findById(userId).get();
                return new ResponseEntity<>(user, HttpStatus.OK);
            }
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        catch (Exception ex){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping
    public @ResponseBody ResponseEntity addUser(@RequestBody String userdata){
        try{
            User[] users = gson.fromJson(userdata, User[].class);
            for(User user : users){
                userRepository.save(user);
            }
            return new ResponseEntity(HttpStatus.OK);
        }
        catch (Exception ex){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping
    public @ResponseBody ResponseEntity deleteUser(@RequestBody Long id){
        try{
            userRepository.deleteById(id);
            return new ResponseEntity(HttpStatus.OK);
        }
        catch (Exception ex){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @PatchMapping
    public @ResponseBody ResponseEntity updateUser(@RequestBody String userdata){
        User user = gson.fromJson(userdata, User.class);
        try{
            userRepository.save(user);
            return new ResponseEntity(HttpStatus.OK);
        }
        catch (Exception ex){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }
}
