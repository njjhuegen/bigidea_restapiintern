package org.springframework.internalrestapi.controller;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.internalrestapi.models.Campaign;
import org.springframework.internalrestapi.repository.CampaignRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:8080")
@RestController
@RequestMapping("/campaign")
public class CampaignController {
    @Autowired
    CampaignRepository campaignRepository;
    private Gson gson = new Gson();


    @GetMapping("/{user}")
    public ResponseEntity getAllMyCampaigns(@PathVariable String user) {
        List<Campaign> campaigns;
        try{
            campaigns = campaignRepository.findAllMyCampaignsByUserId(Long.parseLong(user));
            if(campaigns.isEmpty()){
                return new ResponseEntity(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(campaigns, HttpStatus.OK);
        } catch (Exception ex){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping("{user}/{camp}")
    public ResponseEntity getCampaign(@PathVariable Long camp, @PathVariable Long user) {
        Campaign campaign;
        try{
            campaign = campaignRepository.getOneByCampaignId(user, camp);
            if(campaign == null){
                return new ResponseEntity(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(campaign, HttpStatus.OK);
        } catch (Exception ex){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @PatchMapping("")
    public @ResponseBody ResponseEntity updateCampaign(@RequestBody String campaign){
        try {
            Campaign campaignModel = gson.fromJson(campaign, Campaign.class);
            campaignRepository.save(campaignModel);
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception ex){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("")
    public @ResponseBody ResponseEntity createCampaign(@RequestBody String campaign){
        try {
            Campaign campaignModel = gson.fromJson(campaign, Campaign.class);
            campaignRepository.save(campaignModel);
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception ex){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("")
    public @ResponseBody ResponseEntity deleteCampaign(@RequestBody String campaign){
        try{
            Campaign campaignModel = gson.fromJson(campaign, Campaign.class);
            campaignRepository.deleteOne(campaignModel.getId(), campaignModel.getDm().getId());
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception ex){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }
}
