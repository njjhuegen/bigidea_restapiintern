package org.springframework.internalrestapi.models;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "spells")
@EntityListeners(AuditingEntityListener.class)
public class Spell {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @NotBlank
    private String name;

    @NotBlank
    private Long level;

    @NotBlank
    private String type;

    @NotBlank
    private String casting_time;

    @NotBlank
    private String spell_range;

    @NotBlank
    private String components;

    @NotBlank
    private String duration;

    @NotBlank
    private String description;

    @NotBlank
    private String higher_levels;

    @ManyToMany
    @JoinTable(
            name = "classes_spells",
            joinColumns = @JoinColumn(name = "spell_id"),
            inverseJoinColumns = @JoinColumn(name = "class_id")
    )
    private List<Architype> architypes;

}
