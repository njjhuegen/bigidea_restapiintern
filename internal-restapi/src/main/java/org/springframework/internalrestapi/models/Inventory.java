package org.springframework.internalrestapi.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;
import reactor.util.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "inventory")
@EntityListeners(AuditingEntityListener.class)
public class Inventory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @OneToMany(fetch = FetchType.LAZY)
    @JsonIgnoreProperties
    @Nullable
    private List<Item> items = new ArrayList<>();

    @NotNull(message = "copper amount can not be blank")
    private Long copper_amount;

    @NotNull(message = "silver amount can not be blank")
    private Long silver_amount;

    @NotNull(message = "gold amount can not be blank")
    private Long gold_amount;

    @NotNull(message = "platinum amount can not be blank")
    private Long platinum_amount;

}
