package org.springframework.internalrestapi.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "proficiency")
@EntityListeners(AuditingEntityListener.class)
public class Proficiency {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "proficiency_id", updatable = false, nullable = false)
    private Long id;

    @JsonProperty("name")
    @NotBlank
    private String name;

    @JsonProperty("type")
    @NotBlank
    private String type;

}
