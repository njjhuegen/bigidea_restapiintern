package org.springframework.internalrestapi.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;


@Getter
@Setter
@Entity
@Table(name = "classes")
@EntityListeners(AuditingEntityListener.class)
public class Architype {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @Column(unique = true)
    @JsonProperty("name")
    private String name;

    @JsonProperty("hit_die")
    private Long hit_die;

}
