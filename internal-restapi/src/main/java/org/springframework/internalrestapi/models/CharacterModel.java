package org.springframework.internalrestapi.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "player_character")
@EntityListeners(AuditingEntityListener.class)
public class CharacterModel {

    @Id
    @JsonIgnoreProperties
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private User user_id;

    @OneToOne(fetch = FetchType.EAGER, orphanRemoval = true, cascade = CascadeType.ALL)
    @JoinColumn(name = "inventory_id")
    private Inventory inventory_id;

    @OneToOne(fetch = FetchType.EAGER, orphanRemoval = true, cascade = CascadeType.ALL)
    @JoinColumn(name = "stat_id")
    private StatsModel stat_id;

    @NotBlank(message = "Name must snot be blank")
    private String name;

    @NotBlank(message = "architype must not be blank")
    private String architype;

    @NotBlank(message = "race must not be empty")
    private String race;

    @NotNull(message = "current must not be empty")
    private Long current_hp;

    @NotNull(message = "max hp can not be 0 and must not be empty")
    private Long max_hp;

    @NotBlank(message = "background must not be empty")
    private String background;

    @NotBlank(message = "Alignment must not be empty")
    private String alignment;

    @NotBlank(message = "Religion must not be empty")
    private String religion;

    @NotNull(message = "xp must not be empty")
    private Long xp;

    private String subArchitype;

    @NotNull(message = "Death_success must not be empty")
    private Long death_Succes;

    @NotNull(message = "Death_failure must not be empty")
    private Long death_failure;

}
