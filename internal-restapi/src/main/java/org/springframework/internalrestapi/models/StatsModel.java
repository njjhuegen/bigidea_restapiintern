package org.springframework.internalrestapi.models;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@Entity
@Table(name = "character_stats")
@EntityListeners(AuditingEntityListener.class)
public class StatsModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;

    @NotNull(message = "Strength can not be blank")
    private Long str_val;

    @NotNull(message = "Dexterity can not be blank")
    private Long dex_val;

    @NotNull(message = "Constitution can not be blank")
    private Long cons_val;

    @NotNull(message = "Intelligence can not be blank")
    private Long int_val;

    @NotNull(message = "Wisdom can not be blank")
    private Long wis_val;

    @NotNull(message = "Charisma can not be blank")
    private Long cha_val;

}
