package org.springframework.internalrestapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.internalrestapi.models.Campaign;
import org.springframework.internalrestapi.models.CharacterModel;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CampaignRepository extends JpaRepository<Campaign, Long> {
    @Query(value = "SELECT u FROM Campaign u INNER JOIN u.characters c WHERE c.user_id.id = ?1 OR u.dm = ?1")
    List<Campaign> findAllMyCampaignsByUserId(Long id);

    @Query(value = "SELECT u FROM Campaign u INNER JOIN u.characters c WHERE c.user_id.id = ?1 AND u.id = ?2 OR u.dm = ?1 ")
    Campaign getOneByCampaignId(Long user, Long id);

    @Query(value = "UPDATE Campaign u SET u.name = ?1, u.characters = ?2 WHERE u.id = ?3")
    Void updateOne(String name, List<CharacterModel> characters, Long cmpid);

    @Query(value = "DELETE FROM Campaign u WHERE u.id = ?1 AND u.dm.id = ?2")
    Void deleteOne(Long id, Long user);
}
