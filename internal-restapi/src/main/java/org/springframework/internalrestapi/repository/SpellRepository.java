package org.springframework.internalrestapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.internalrestapi.models.Spell;

import java.util.List;

public interface SpellRepository extends JpaRepository<Spell, Long> {
    @Query(value = "SELECT u FROM Spell u WHERE u.name = ?1")
    Spell getSpellByName(String name);

    @Query(value = "SELECT u FROM Spell u INNER JOIN u.architypes a WHERE a.name = ?1")
    List<Spell> getSpellsByClassName(String name);

    @Query(value = "SELECT u FROM Spell u INNER JOIN u.architypes a WHERE a.id = ?1")
    List<Spell> getSpellByClassId(Long id);
}
