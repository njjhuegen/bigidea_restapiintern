package org.springframework.internalrestapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.internalrestapi.models.StatusAilment;

public interface StatusAilmentRepository extends JpaRepository<StatusAilment, Long> {
    @Query(value = "SELECT u FROM StatusAilment u WHERE u.name = ?1")
    StatusAilment getStatusailmentByName(String name);
}
