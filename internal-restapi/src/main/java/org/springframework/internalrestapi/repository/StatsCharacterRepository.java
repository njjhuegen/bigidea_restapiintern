package org.springframework.internalrestapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.internalrestapi.models.StatsModel;
import org.springframework.stereotype.Repository;

@Repository
public interface StatsCharacterRepository extends JpaRepository<StatsModel, Long> {
}
