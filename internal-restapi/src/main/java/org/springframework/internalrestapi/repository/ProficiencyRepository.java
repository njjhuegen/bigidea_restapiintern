package org.springframework.internalrestapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.internalrestapi.models.Proficiency;

public interface ProficiencyRepository extends JpaRepository<Proficiency, Long> {
    @Query(value = "SELECT u FROM Proficiency u WHERE u.name = ?1")
    Proficiency getProficiencyByName(String name);
}
