package org.springframework.internalrestapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.internalrestapi.models.Architype;
import org.springframework.stereotype.Repository;

@Repository
public interface ArchitypeRepository extends JpaRepository<Architype, Long> {
    @Query(value = "SELECT u FROM Architype u WHERE u.name = ?1")
    Architype getClassByName(String name);
}
