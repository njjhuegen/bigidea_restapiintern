package org.springframework.internalrestapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.internalrestapi.models.CharacterModel;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CharacterRepository extends JpaRepository<CharacterModel, Long> {
    @Query(value = "SELECT u FROM CharacterModel u WHERE u.user_id.id = ?1")
    List<CharacterModel> findAllCharacters(Long id);

    @Query(value = "SELECT u FROM CharacterModel u WHERE u.user_id.id = ?1 AND u.id = ?2")
    CharacterModel getOneCharById(Long id, Long charid);
}
